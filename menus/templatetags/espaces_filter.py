from django import template


register = template.Library()


@register.filter
def espaces_filter(children):

    if children and children[0].menu_level != 0:
        return children

    espace = False

    for child in children:
        if (child.ancestor or child.selected) and child.get_menu_title()[:7] == 'Espace ':
            espace = True

    new_children = []

    if espace:
        for child in children:
            if child.get_menu_title()[:7] == 'Espace ':
                new_children.append(child)
    else:
        for child in children:
            if child.get_menu_title()[:7] != 'Espace ':
                new_children.append(child)

    return new_children


@register.filter
def espaces_only(children):

    return [child for child in children if child.get_menu_title()[:7] == 'Espace ']
