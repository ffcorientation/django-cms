from django.conf import settings
from django.utils.translation import string_concat, ugettext_lazy
from django.utils.html import strip_tags

from haystack import indexes

from cms.models.pagemodel import Page
from cms.models.pluginmodel import CMSPlugin
from cms.plugins.file.models import File

from subprocess import Popen, PIPE
from os.path import join


class PageIndex(indexes.SearchIndex, indexes.Indexable):

    text = indexes.CharField(document=True, use_template=False)
    pub_date = indexes.DateTimeField(model_attr='publication_date')
    login_required = indexes.BooleanField(model_attr='login_required')
    url = indexes.CharField(stored=True, indexed=False, model_attr='get_absolute_url')
    title = indexes.CharField(stored=True, indexed=False, model_attr='get_title')
    site_id = indexes.IntegerField(model_attr="site_id")

    def get_model(self):

        return Page

    def prepare(self, obj):

        self.prepared_data = super(PageIndex, self).prepare(obj)
        plugins = CMSPlugin.objects.filter(placeholder__in=obj.placeholders.all())
        text = []
        for plugin in plugins:
            instance, _ = plugin.get_plugin_instance()
            if hasattr(instance, 'search_fields'):
                text += [strip_tags(getattr(instance, field, '')) for field in instance.search_fields]
            if getattr(instance, 'search_fulltext', False):
                text += [strip_tags(instance.render_plugin())]
        self.prepared_data['text'] = ' '.join(text)
        return self.prepared_data

    def index_queryset(self, using=None):

        qs = Page.objects.filter(published=True).distinct()
        if 'publisher' in settings.INSTALLED_APPS:
            qs = qs.filter(publisher_is_draft=True)
        return qs


class FileIndex(indexes.SearchIndex, indexes.Indexable):

    text = indexes.CharField(document=True, use_template=False)
    site_id = indexes.IntegerField(model_attr="page__site_id")

    def get_model(self):

        return File

    def prepare_text(self, obj):

        tika_path = join(settings.PROJECT_ROOT, 'tika/tika-app-1.0.jar')
        p = Popen(['java', '-jar', tika_path, '-t', '-eutf8', obj.file.path], stdout=PIPE)
        return obj.title + '\n' + p.stdout.read().decode('utf-8')
        return obj.title

    def index_queryset(self, using=None):

        return File.objects.filter(placeholder__page__isnull=False)
