# -*- coding: utf-8 -*-
from __future__ import absolute_import
from django.conf import settings
from django.core.management.base import NoArgsCommand, CommandError
from cms.models import GlobalPagePermission, PagePermission, Page
from django.contrib.auth.models import Group
from django.contrib.sites.models import Site
from django.db import transaction

class Command(NoArgsCommand):

    @transaction.commit_on_success
    def handle_noargs(self, **options):
        for site in Site.objects.filter(id__gt=10000):
            self.update(site)

    def update(self, site):
        n = site.id - 10000
        group = Group.objects.get(name='cms%u' % n)
        perm = GlobalPagePermission.objects.create(
            can_publish=True,
            group=group,
            can_change_advanced_settings=True,
            can_moderate=True,
            can_change=True,
            can_change_permissions=False,
            can_recover_page=True,
            can_view=False,
            can_add=True,
            user=None,
            can_delete=True,
            can_move_page=True
        )
        perm.sites.add(site)
        page = Page.objects.get(
            title_set__menu_title=u'Découvrir la CO',
            site=site
        )
        PagePermission.objects.create(
            can_publish=False,
            group=group,
            can_change_advanced_settings=False,
            can_moderate=False,
            can_change=False,
            can_change_permissions=False,
            page=page,
            can_view=False,
            can_add=False,
            user=None,
            can_delete=False,
            grant_on=5,
            can_move_page=False
        )
