from django.db import models
from django.utils.translation import ugettext_lazy as _
from cms.models import CMSPlugin, Page
from zinnia.models.category import Category
from licences.models import Licencie

class Link(CMSPlugin):
    """
    A link to an other page or to an external website
    """

    name = models.CharField("texte", max_length=256)
    url = models.URLField("page externe", verify_exists=False, blank=True, null=True)
    page_link = models.ForeignKey(Page, blank=True, null=True)
    mailto = models.EmailField("adresse email", blank=True, null=True)
    target = models.CharField(_("target"), blank=True, max_length=100, choices=((
        ("", _("same window")),
        ("_blank", _("new window")),
        ("_parent", _("parent window")),
        ("_top", _("topmost frame")),
    )))
    zinnia_link = models.ForeignKey(Category, verbose_name='liste d\'actus', blank=True, null=True)
    bio_link = models.ForeignKey(Licencie, verbose_name='bio', blank=True, null=True)
    
    def __unicode__(self):
        return self.name

    search_fields = ('name',)
