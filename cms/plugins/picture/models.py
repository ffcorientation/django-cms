# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _
from cms.models import CMSPlugin, Page
from os.path import basename

class Picture(CMSPlugin):
    """
    A Picture with or without a link
    """
    CENTER = "center"
    LEFT = "left"
    RIGHT = "right"
    FLOAT_CHOICES = ((CENTER, _("center")),
                     (LEFT, _("left")),
                     (RIGHT, _("right")),
                     )
    
    
    image = models.ImageField(_("image"), upload_to=CMSPlugin.get_media_path)
    url = models.CharField("Lien externe", max_length=255, blank=True, null=True, help_text="Lien vers une page externe au site FFCO (facultatif).")
    page_link = models.ForeignKey(Page, verbose_name="Lien interne", null=True, blank=True, help_text="Lien vers une page du site FFCO (facultatif).")
    longdesc = models.CharField("Légende", max_length=255, blank=True, null=True, help_text="Si renseigné, l'image sera encadrée d'un filet.")
    float = models.CharField("placement", max_length=10, blank=True, null=True, choices=FLOAT_CHOICES, help_text="----- = dans le flux, gauche/droite = habillage par le texte, centre = pas d'habillage.")
    
    def __unicode__(self):
        if self.image:
            # added if, because it raised attribute error when file wasn't defined
            try:
                return u"%s" % basename(self.image.path)
            except:
                pass
        return "<empty>"
