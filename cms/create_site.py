# -*- coding: utf-8 -*-

from cms.models.pagemodel import Page
from cms.models.permissionmodels import PageUser, GlobalPagePermission
from cms.utils.moderator import update_moderation_message
from cms.exceptions import NoHomeFound
from cms.models.managers import PageManager, PagePermissionsPermissionManager
from cms.models.metaclasses import PageMetaClass
from cms.models.placeholdermodel import Placeholder
from cms.models.pluginmodel import CMSPlugin
from cms.publisher.errors import MpttPublisherCantPublish
from cms.utils import i18n, urlutils, page as page_utils
from cms.utils.copy_plugins import copy_plugins_to
from cms.utils.helpers import reversion_register
from datetime import datetime
from django.conf import settings
from django.contrib.sites.models import Site
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models import Q
from django.shortcuts import get_object_or_404
from django.utils.translation import get_language, ugettext_lazy as _
from menus.menu_pool import menu_pool
from mptt.models import MPTTModel
from os.path import join
import copy
from cms.api import create_page_user, assign_user_to_page
from django.db import transaction
from django.db.models import Q
from django.contrib.auth.models import Group
from licences.models import Structure, Comite, Ligue
from cms.models.titlemodels import Title
from cms.plugins.text.models import Text
from cms.utils.permissions import get_current_user


@transaction.commit_on_success
def create_site(to_site, from_site=None):

    group_name = 'cms' + str(to_site.id - 10000)
    group, created = Group.objects.get_or_create(name=group_name)

    if not from_site:
        from_site = Site.objects.get(id=10000)

    todo = (
        (u'Accueil', False),
        (u'La CO c\'est quoi ?', False),
        (u'La CO pour les jeunes', True),
        (u'Qui sommes-nous ?', True),
        (u'Les courses', True),
        (u'Espace licencié', True),
        (u'Espace collectivité', True),
        (u'Espace presse', True),
        (u'Espace partenaire', True),
        (u'Mentions légales', False),
        (u'Crédits photo', False),
    )

    for title, add_rights in todo:
        page = Page.objects.get(title_set__title=title, site=from_site)
        page.copy_page(None, to_site)
        if not add_rights:
            from cms.models.permissionmodels import PagePermission
            PagePermission(page=page, group=group, can_change=False, can_add=False, can_delete=False, can_change_advanced_settings=False, can_publish=False, can_change_permissions=False, can_move_page=False).save()

    perm = GlobalPagePermission.objects.create(
        can_publish=True,
        group=group,
        can_change_advanced_settings=True,
        can_moderate=True,
        can_change=True,
        can_change_permissions=False,
        can_recover_page=True,
        can_view=False,
        can_add=True,
        user=None,
        can_delete=True,
        can_move_page=True
    )
    perm.sites.add(to_site)

    Page.objects.filter(site=to_site).update(published=True)
    structure = Structure.objects.get(id=to_site.id-10000).downcast()
    for title in Title.objects.filter(Q(title__contains='#dutypestruct#') | Q(title__contains='#structure#'), page__site=to_site):
        if isinstance(structure, Comite):
            title.title = title.title.replace('#dutypestruct#', 'du CD')
            title.title = title.title.replace('#structure#', 'CDCO' + structure.numero)
        elif isinstance(structure, Ligue):
            title.title = title.title.replace('#dutypestruct#', 'de la ligue')
            title.title = title.title.replace('#structure#', 'L' + structure.code + 'CO')
        title.save()
    for text in Text.objects.filter(placeholder__page__site=to_site, body__contains='#dutypestruct#'):
        if isinstance(structure, Comite):
            text.body = text.body.replace('#dutypestruct#', 'du CD')
        elif isinstance(structure, Ligue):
            text.body = text.body.replace('#dutypestruct#', 'de la ligue')
        text.save()

    menu_pool.clear(site_id=to_site.pk)
